let selects = $( ".selecteur_loi" );
for (let i=0; i< selects.length; i++){
	console.log(i);
    let select = document.getElementById('select_loi_' + i);
    select.addEventListener("click", (event) => {
        let loi = select.options[select.selectedIndex].value;
        document.getElementById("image_circulaire_" + i).src = "static/images/plot_circulaire_" + loi + "_" + i + ".png";
    })
}

let selecteurs_unites = $( ".selecteur_unite" );
for (let i=0; i< selecteurs_unites.length; i++){
	
    let select = document.getElementById('select_unit_' + i);
    select.addEventListener("click", (event) => {
        let unite = select.options[select.selectedIndex].value;
		$.get( "./api/resumes_circulaires/" + i + "/" + unite, function( data ) {
			//$( ".result" ).html( data );
			//alert( "Load was performed." );
			$( "#resume_circulaire_" + i ).text(data);
		});

		$.get( "./api/moyenne_unite/" + i + "/" + unite, function( data ) {
			//$( ".result" ).html( data );
			//alert( "Load was performed." );
			data = data.replace("[", "");
			data = data.replace("]", "");
			var array = data.split(",");
			for (let y=0; y< array.length; y++){
				index = i + $(".continu").length;
				$( "#cellule_circulaire_" + y + "_" + index  ).text(array[y].replace("'", "").replace("'", ""));
			}
		});
        //document.getElementById("image_circulaire_" + i).src = "static/images/plot_circulaire_" + loi + "_" + i + ".png";
    });
}

/* CAROUSSEL */

// initialisation

function caroussel(divs_c, nom_bouton){
	if (divs_c.length > 0){
		let current_c = 0;
		let button_c_n = document.getElementById(nom_bouton + "_next");
		let button_c_p = document.getElementById(nom_bouton + "_previous");

		// Définir la visibilité sur faux

		for (let i=1; i< divs_c.length; i++){
			divs_c[i].style.display='none';
		}

		// Mettre le premier visible par défaut

		divs_c[0].style.display='flex';

		button_c_n.addEventListener("click", (event) => {
			if (current_c < divs_c.length - 1){

				current_c ++;
				divs_c[current_c - 1].style.display='none';
				divs_c[current_c].style.display='flex';
			}
		});

		button_c_p.addEventListener("click", (event) => {
			if (current_c > 0){
				current_c --;
				divs_c[current_c + 1].style.display='none';
				divs_c[current_c].style.display='flex';
			}
		});

	}
}


let divs_c = $("div[id^='div_circulaire_']");



let divs_d = $("div[id^='div_discrete_']");
let divs_con = $("div[id^='div_continue_']");

caroussel(divs_c, "circular");
caroussel(divs_d, "discrete");
caroussel(divs_con, "continue");







