import pandas as pd
import os
from python_classes.graphe import DiGraphe
from python_classes.plots import Plots
import json

# Ajout du dossier bin aux variables d'environnemen

os.environ["PATH"] += ";.."
os.environ["PATH"] += ";.\\bin"


"""
+++++++++++++++++++++++++++++++++++++++++++++++
                   ZONE API
+++++++++++++++++++++++++++++++++++++++++++++++
"""


def get_resume(id_variable: str, unite: str):
    """
    FONCTION DE L'API
    Permet de récupérer le résumé qui correspond aux paramètres

    Args:
        id_variable (str): ID de la variable circulaire (0 à n)
        unite (str): Unité souhaiter pour le résumé

    Returns:
        str: résumé 
    """
    with open('out/resumes_circulaires.json', 'r', encoding="utf8") as f:
        data = json.load(f)
    return data[id_variable][unite]


def get_moyenne_unite(id_variable: str, unite: str):
    """
    FONCTION DE L'API
    Permet de récupérer la liste des moyennes d'une variable circulaire au format souhaité 

    Args:
        id_variable (str): ID de la variable circulaire (0 à n)
        unite (str): Unité souhaiter pour le résumé

    Returns:
        list[str]: liste des moyennes
    """
    with open('out/resumes_circulaires_moyennes.json', 'r', encoding="utf8") as f:
        data = json.load(f)
    return str(data[id_variable][unite])



"""
+++++++++++++++++++++++++++++++++++++++++++++++
                 ZONE MARKOV
+++++++++++++++++++++++++++++++++++++++++++++++
"""


def sauvegarder_graphe(A_file, format):
    """Permet de créer la chaine de markov et de l'enregistrer au format choisi

    Args:
        A_file (_type_): Chemin vers le fichier markov.csv
        format (_type_): Format auquel on souhaite enregistrer la chaine de markov 

    Returns:
        (list, Graph): (liste des couleurs, graphe (objet))
    """

    # Dataframe qui contient la matrice de transition
    A_df = pd.read_csv(A_file)

    # Instanciation et sauvegarde du graphe 
    graphe = DiGraphe(A_df, proba_mini=0.000001, proba_faible=0.1)
    graphe.enregistrer_PNG("", "./static/images/")

    return graphe.get_couleurs_01(), graphe


"""
+++++++++++++++++++++++++++++++++++++++++++++++
                 ZONE PLOTS
+++++++++++++++++++++++++++++++++++++++++++++++
"""


def sauvegarder_plot(couleurs):
    """Permet de sauvegarder l'ensemble des plots

    Args:
        couleurs (list): Couleurs du graphe pour garder les mêmes 

    Returns:
        Plots: Objet qui contient toutes les informations sur les plots
    """

    # On vérifie si les fichiers sont présents pour ne pas générer d'erreur
    if "continue.csv" in os.listdir("data"):
        df_continue = pd.read_csv("data/continue.csv")
    else:
        df_continue = None

    if "circulaire.csv" in os.listdir("data"):
        df_circulaire = pd.read_csv("data/circulaire.csv")
    else:
        df_circulaire= None

    if "discrete.csv" in os.listdir("data"):
        df_discrete = pd.read_csv("data/discrete.csv")
    else:
        df_discrete= None

    # Instaciation et sauvegarde des différents plots 
    plots = Plots(couleurs, df_continue, df_circulaire, df_discrete)
    plots.enregistrer("png")

    return plots


def noms_plots(n: int, type: str, lois_circulaire=None):
    """Permet de générer les chemins vers les images des plots (utile pour le HTML)

    Args:
        n (int): Nombre de plots
        type (str): Type des plots (circulaire, continue, discret ?)
        lois_circulaire (_type_, optional): Liste des lois circulaires par défaut. Defaults to None.

    Returns:
        list: Liste des chemin vers les images
    """
    noms = []
    for i in range(n):
        # Dans le cas des plots circulaires
        if lois_circulaire is not None:
            nom = f"/images/plot_{type}_{lois_circulaire[i]}_{i}.png"
        # Dans le cas "normal"
        else:
            nom = f"/images/plot_{type}_{i}.png"
        noms.append(nom)
    return noms



def hexa_couleurs(couleurs):
    # Définition des couleurs
    nouvelles_couleurs = []
    for couleur in couleurs:
        # On convertit les couleurs en hexadécimale pour le plot
        nouvelles_couleurs.append('#%02x%02x%02x' % (int(couleur[0] * 255), int(couleur[1] * 255), int(couleur[2] * 255)))
    return nouvelles_couleurs




