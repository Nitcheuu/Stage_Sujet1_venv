let selectedFile;

document.getElementById('input_B').addEventListener("change", (event) => {
    let variables = [];
    // Sélection du fichier de l'input
    selectedFile = event.target.files[0];
    // SI il y a un fichier
    if(selectedFile){
        // Lecture du fichier en mode binaire
        let fileReader = new FileReader();
        fileReader.readAsBinaryString(selectedFile);
        // Début de la lecture du fichier
        fileReader.onload = (event)=>{
             // target.result contient le résultat de la lecture
             let data = event.target.result;
             // Lecture au format XLSX à l'aide de la bibliothèque
             let workbook = XLSX.read(data,{type:"binary"});
             // Séparation des lignes
             let rowObject;
             workbook.SheetNames.forEach(sheet => {
                  rowObject = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheet]);
             });
             // Pour chaque ligne
             for (let i=0; i < rowObject.length; i++){
               // Si il y a une séparateur
               if (rowObject[i][1] === "//"){
                   // Alors il y a une variable
                   variables.push(rowObject[i][0]);
               }
             }
             console.log(variables.length);
             console.log("test");
             if (variables.length === 0){

                 // Affichage du message d'erreur
                document.getElementById("erreur_0").style.visibility = "visible";
                document.getElementById("variables").style.visibility = "hidden";
                //document.getElementById("variables").style.visibility = "hidden";
             }else{
                 // On cache le message d'erreur
                 document.getElementById("erreur_0").style.visibility = "hidden";
                 document.getElementById("variables").style.visibility = "visible";
                 /*document.getElementById("variable_0").textContent = variables[0];*/
                 let html_div = "";
                 for(let i=0; i < variables.length; i++){
                     html_div += "<div style=\"display: flex; align-items: center; justify-content: center;\">";
                     html_div += '<p style="width: 70px;">' + variables[i] + '</p>';
                     html_div += '<select name="variable_' + i +'">\n' +
                         '    <option value="continue">Continue</option>\n' +
                         '    <option value="discrete">Discrète</option>\n' +
                         '    <option value="circulaire">Circulaire</option>\n' +
                         '</select></div><br>'
                 }
                 document.getElementById("variables").innerHTML += html_div + "<button id=\"bouton_1\"  type=\"submit\"  style=\"display: block; margin-right: auto; margin-left: auto;\">Envoyer</button>";
             }


        }
    }

});


