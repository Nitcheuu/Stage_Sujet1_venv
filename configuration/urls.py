from django.urls import *
from . import views

urlpatterns = [
    path('',  views.home),
    path('loi/<str:loi>', views.changer_loi),
    path('api/resumes_circulaires/<str:id_variable>/<str:unite>', views.get_resumes),
    path('api/moyenne_unite/<str:id_variable>/<str:unite>', views.get_moyennes_unite)
]