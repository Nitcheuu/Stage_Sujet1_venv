from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpRequest
from .utils import *
import pandas as pd
import os
# Create your views here.


def changer_loi(request: HttpRequest, loi: str):
    global loi_proba
    set_loi(loi)
    #loi_proba = get_loi()
    return redirect("/")


def get_resumes(request: HttpRequest, id_variable: str, unite: str):
    """API : Permet de renvoyer les résumés des variables circulaires en fonction des arguments

    Args:
        request (HttpRequest): Requête HTTP
        id_variable (str): ID de la variable circulaire (0 à n)
        unite (str): Unité avec laquelle on souhaite interpréter le résumé (jour, heure, etc)

    Returns:
        _type_: Le résumé (en HTPP)
    """
    return HttpResponse(get_resume(id_variable, unite))


def get_moyennes_unite(request: HttpRequest, id_variable: str, unite: str):
    """API : Permet de renvoyer la liste des moyennes de la variable circulaire choisie (avec l'unité choisie)

    Args:
        request (HttpRequest): Requête HTTP
        id_variable (str): ID de la variable circulaire (0 à n)
        unite (str): Unité avec laquelle on souhaite récupérer les moyennes (jour, heure, angle, etc)

    Returns:
        _type_: Liste des moyennes avec la bonne unité (en HTTP)
    """
    return HttpResponse(get_moyenne_unite(id_variable, unite))


def home(request : HttpRequest):
    """ Vue principale de l'application

    Args:
        request (HttpRequest): Requête HTTP
    """

    # Dictionnaire qui va contenir toutes les variables que l'on souhaite données au template
    data = {}

    # Si l'utilisateur a introduit le fichier markov dans le dossier data
    if "markov.csv" in os.listdir("data"):
        # Chargement du graphe et récupération des couleurs 
        couleurs, graphe = sauvegarder_graphe("data/markov.csv", "csv")
    else:
        # Sinon on définit des couleurs par défaut
        couleurs = ["red", "green", "blue", "cyan", "magenta", "yellow", "black", "red", "green", "blue", "cyan", "magenta", "yellow", "black"]

    # Chargement des différents plots, on respecte le code couleur
    plots = sauvegarder_plot(couleurs)

    data = {
        "plots": plots, # On passe la classse qui modélise les différents plots
        # On passe les chemins vers les images des plots
        "plots_continues": noms_plots(len(plots.nom_variables["continue"]), "continue"),
        "plots_circulaires": noms_plots(len(plots.nom_variables["circulaire"]), "circulaire", lois_circulaire=plots.defaut_circulaire),
        "plots_discrets": noms_plots(len(plots.nom_variables["discrete"]), "discrete"),
        # On passe les lois par défaut pour chaque plot 
        "loi_defaut": plots.defaut_circulaire,
        # Booléen qui permet de savoir si on doit charger la partie de l'application avec la chaîne de Markov
        "markov": "markov.csv" in os.listdir("data"),
        # On passe les résumés des variables circulaires
        "resumes_circulaires": plots.resume_variables["circulaire_defaut"],
        # Le nom de chacune des variables circulaires
        "noms_circulaires": plots.nom_variables["circulaire"],
        "resumes_discrets": plots.resume_variables["discrete"],
        "noms_discrets": plots.nom_variables["discrete"],
        "noms_discrets_valeurs": plots.nom_variables["discrete_valeurs"],
        "resumes_continus": plots.resume_variables["continue"],
        "noms_continus": plots.nom_variables["continue"],
        # Palette de couleur des différents états 
        "couleurs" : plots.couleurs,
        "resume_global_colonnes": plots.resume_variables["global_colonnes"],
        "resume_global": plots.resume_variables["global"],
    }

    data["couleurs"] = hexa_couleurs(data["couleurs"]) if len(data["noms_discrets"]) == 0 else data["couleurs"]

    # On passe ici en paramètre la position des variables
    # modifier les unités des moyennes dans le tableau de résumé 
    if len(plots.nom_variables["circulaire"]) > 0:
        data["indice_debut_circulaire"] = len(plots.nom_variables["continue"]) - 1
        data["indice_fin_circulaire"] = len(plots.nom_variables["continue"]) + len(plots.nom_variables["circulaire"])

    return render(request, 'configuration/resultat.html', data)


    """elif not data["uploads"][0] or not data["uploads"][1]:
        return render(request, 'configuration/configuration.html', data)"""